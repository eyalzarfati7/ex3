<?php
class Htmlpage{
    protected $title = "ex3";
    protected $body = "ex3";
    public function view(){
     echo "<html>
           <head>
              <title>$this->title</title>
           </head>

           <body>
               $this->body    
           </body>    
           </html>";               
    }
    function __construct($title = "",$body = ""){
        if($title != ""){
            $this->title=$title;
        }
        if($body != ""){
            $this->body=$body;
        }
    }

}

class colorMessage extends Htmlpage {
    protected $color = 'red';
    public function __set($property,$value){
        if ($property == 'color'){
            $colors = array('red','yellow','green');
            if(in_array($value, $colors)){
                $this->color = $value;
            }
        
            else{
                $this->body= "Eror, please choose another color";
            }
        }
    }
    public function view(){
        echo "<p style = 'color:$this->color'>$this->body</p>";
    }
}
?>
<?php
class fontSize extends colorMessage{
    protected $size;
    public function __set($property,$value){
        if ($property =='size'){
            $sizes = range(10,24); 
            if(in_array($value,$sizes)){
                $this->size = $value;
        }
            else{
                $this->body= "Eror, please choose another size";
                }  
    }
        elseif($property == 'color'){
            parent::__set($property,$value);
        }
    } 
    public function view(){
        echo "<html>
        <head><title>$this->title</title></head>
        <body><p style = 'font-size:$this->size; color:$this->color'> $this->body<p></body>
        </html>";
        }   
    }

?>